package model.data_structures;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface ILinkedList<T> extends Iterable<T> {
    /**
     * M�todo que da el tama�o de la lista
     * @return el tama�o de la lista
     */
	Integer getSize();
	
	void add(T dato, Comparable<T> comparacion);
	
	void addAtEnd(T dato, Comparable<T> comparacion);
	
	void addAtK(int k, T dato, Comparable<T> comparacion);
	
	T getElement(int k);
	
	T getCurrentElement();
	
	void delete();
	
	void deleteAtk(int k);
	
	T next();
	
	T previous();
	

}
