package model.data_structures;

import java.util.Iterator;

public class LinkedList<T extends Comparable<T>> implements ILinkedList<T> {
	// -----------------------------------------------------------------
    // Constantes
    // -----------------------------------------------------------------
    // -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------
	private int longitud;
	
	private T primero;
	
	private T siguiente;
	
	private T actual;
	
	private T anterior;
	// -----------------------------------------------------------------
    // Constructores
    // -----------------------------------------------------------------
	public LinkedList(T pPrimero, Comparable<T> comparacion){
		primero = pPrimero;
		actual = pPrimero;
		longitud = 1;
		siguiente = null;
		anterior = null;
	}
	// -----------------------------------------------------------------
    // M�todos
    // -----------------------------------------------------------------

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		Iterator<T> retorno = (Iterator<T>) primero;
		if (retorno.hasNext()){
			retorno = (Iterator<T>) siguiente;
			actual = (T) retorno;
		}
		else{
			System.out.println("No hay m�s objetos en la lista");
		}
		return retorno;
	}

	@Override
	public Integer getSize() {
		// TODO Auto-generated method stub
		return longitud;
	}

	
	public void add(T dato, Comparable<T> comparacion) {
		// TODO Auto-generated method stub
		// Puedo a�adir al inicio, en el medio o al final
		if(primero == null){
		 primero = (T) new LinkedList(dato, comparacion);
		}
		else{
			//medio
			if(siguiente == null){
			siguiente = (T) new LinkedList(dato, comparacion);
			}
			else{
				Iterator<T> iter = (Iterator<T>) siguiente;
				if(iter.hasNext()){
					add(iter.next(), comparacion);
				}
			}
		}
	}

	public T getElement(int k) {
		// TODO Auto-generated method stub
		int contiter = 0;
		T element = null;
		Iterator<T> iter = (Iterator<T>) primero;
		if(iter.hasNext()){
			iter.next();
			contiter++;
			if (contiter == k){
				element = getCurrentElement();
			}
		}
		return element;
		
	}

	@Override
	public T getCurrentElement() {
		// TODO Auto-generated method stub
		return actual;
	}

	@Override
	public void delete() {
		// TODO Auto-generated method stub
		actual = null;
	}

	@Override
	public void deleteAtk(int k) {
		// TODO Auto-generated method stub
		int contiter = 0;
		Iterator<T> iter = (Iterator<T>) primero;
		if(iter.hasNext()){
			iter.next();
			contiter++;
			if (contiter == k){
				actual = null;
		}
		
	}
	}

	@Override
	public T next() {
		// TODO Auto-generated method stub
		return siguiente;
	}

	@Override
	public T previous() {
		// TODO Auto-generated method stub
		return anterior;
	}

	@Override
	public void addAtEnd(T dato, Comparable<T> comparacion) {
		// TODO Auto-generated method stub
		if(primero == null){
			add(dato, comparacion);
		}
		else{
			Iterator<T> iter = (Iterator<T>) primero;
			dato = iter.next();
			if(siguiente == null){
				add(iter.next(),comparacion);
			}
		}
	}

	@Override
	public void addAtK(int k, T dato, Comparable<T> comparacion) {
		// TODO Auto-generated method stub
		int contiter = 0;
		Iterator<T> iter = (Iterator<T>) primero;
		if(iter.hasNext()){
			iter.next();
			contiter++;
			if (contiter == k){
				actual = dato;
		}
		
	}
	}

	
	

}
