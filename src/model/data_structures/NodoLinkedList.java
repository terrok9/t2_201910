package model.data_structures;

public class NodoLinkedList<T extends Comparable<T>> {

	private LinkedList<T> primero;
	
	private LinkedList<T> siguiente;
	
	private LinkedList<T> anterior;
	
	NodoLinkedList(T elemento){
		primero = (LinkedList<T>) elemento;
		siguiente = null;
		anterior = (LinkedList<T>) elemento;
	}
	public LinkedList<T> darSiguiente(){
		return siguiente;
	}
	public LinkedList<T> darAnterior(){
		return anterior;
				
	}
	public void cambiarSiguiente(){
		siguiente = (LinkedList<T>) siguiente.next();
	}
	
	public void cambiarAnterior(){
		anterior = (LinkedList<T>) anterior.previous();
	}
}
